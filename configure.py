#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, stat, sys
import json
from shutil import which
import urllib.request

from appcli.common.utils import print_line
from appcli.common.apps_conf import (
	SystemLocalDirs,
	AppLocalDirs,
	FilePath,
	FileReader,
	FileJson,
	KERNEL_TYPE,
	HOME,
	shellcore,
)

appname = 'youtube-dl-qt'
default_file_config = os.path.join(HOME, 'youtube-dl-qt.json')
default_app_local_dirs = AppLocalDirs(type_root=False, appname=appname)


class DefaultConfiguration(object):
	def __init__(
			self, *, 
			app_local_dirs:AppLocalDirs=default_app_local_dirs, 
			file_user_config: str = default_file_config) -> None:
		

		self.app_local_dirs: AppLocalDirs = app_local_dirs
		self.app_local_dirs.create_dirs()
		self.file_user_config: str = file_user_config
		self.file_path_prefs: FilePath = FilePath(self.file_user_config)
		self.file_path_json_prefs: FileJson = FileJson(self.file_path_prefs)

		self.youtube_dl_file_bin = 'youtube-dl'
		if KERNEL_TYPE == 'Windows':
			self.youtube_dl_file_bin += '.exe'

		self._preferences = self.get_preferences()
	
	def get_preferences(self) -> dict:

		return {
			"path_videos": self.get_path_download_videos(),
			"path_youtube_dl": self.get_youtube_dl_abspath(),
			"video_format": self.get_video_formats()[0],
		}

	def get_path_download_videos(self) -> str:
		return os.path.join(self.app_local_dirs.system_dirs.dirHome(), 'Downloads')

	def get_youtube_dl_abspath(self) -> str:
		return os.path.join(self.app_local_dirs.system_dirs.dirBin(), self.youtube_dl_file_bin)

	def get_video_formats(self) -> list:
		return ['mp4', 'mkv', 'mp3',]
	
	def get_path_file_preferences(self) -> str:
		return self.file_path_prefs.absolute()
	
	def to_file_json(self) -> None:
		"""
			Salvar as preferências padrão em um arquivo .json
		"""

		self.file_path_json_prefs.write_lines(self.get_preferences())
		
		

class UserPreferences(object):
	'''
	Classe para gerenciar as preferências do usuário em um arquivo .json.
	'''
	def __init__(
			self, *, 
			app_local_dirs: AppLocalDirs=default_app_local_dirs, 
			file_user_config:str=default_file_config):
		
		super().__init__()

		self.app_local_dirs: AppLocalDirs = app_local_dirs
		self.app_local_dirs.create_dirs()
		self.file_user_config: str = file_user_config

		self.default_config: DefaultConfiguration = DefaultConfiguration(
										app_local_dirs=self.app_local_dirs, 
										file_user_config=self.file_user_config
										)
		
		self.file_path_json_user_prefs: FileJson = FileJson(self.default_config.file_path_prefs)

		if not self.default_config.file_path_prefs.exists():
			self.default_config.to_file_json()

		self._preferences = None

	def get_user_prefs(self) -> dict:
		"""
			Ler as configurações do arquivo  preferências
		"""
		if not self.default_config.file_path_prefs.exists():
			return self.default_config.get_preferences()
		
		return self.file_path_json_user_prefs.lines_to_dict()
	
	def get_youtube_dl_abspath(self) -> str:
		return self.get_user_prefs()['path_youtube_dl']
	
	def set_user_preferences(self):
		return self.get_user_prefs()

	def get_json_config(self):
		return self.get_user_prefs()

	def update_preference(self, key: str, value: str):
		'''Atualiza apenas uma preferência no arquivo json'''
		
		if self._preferences is None:
			self._preferences = self.get_user_prefs()

		if not key in self._preferences:
			self._preferences.update({key: value})
		else:
			self._preferences[key] = value

		# Atualizar o arquivo no disco
		try:
			self.file_path_json_user_prefs.update_key(key, value)
		except Exception as err:
			print(__class__.__name__, err)
		print_line()
		print(self._preferences)



class Configure(object):
	'''
	   Classe para configurações básicas do sistema operacional, diretórios
	pastas e arquivos necessários para este programa.
	'''

	def __init__(self, user_prefs: UserPreferences):
		super().__init__()

		self.user_prefs: UserPreferences = user_prefs

		if KERNEL_TYPE == 'Windows':
			self.url_youtube_dl = 'https://yt-dl.org/downloads/2020.07.28/youtube-dl.exe'
			self.url_visual_c = 'https://download.microsoft.com/download/5/B/C/5BC5DBB3-652D-4DCE-B14A-475AB85EEF6E/vcredist_x86.exe'
		elif KERNEL_TYPE == 'Linux':
			self.url_youtube_dl = 'https://yt-dl.org/downloads/latest/youtube-dl'

	# Getter
	@property
	def url_youtube_dl(self):
		return self._url_youtube_dl

	# Setter
	@url_youtube_dl.setter
	def url_youtube_dl(self, url):
		self._url_youtube_dl = url

	def download_youtube_dl_bin(self):
		'''	
		Baixar o youtube-dl para Linux ou Windows.
		'''

		if os.path.isfile(self.user_prefs.get_user_prefs()['path_youtube_dl']) == True:
			# Aterar permissão de execução do arquivo se necessário.
			if os.access(self.path_youtube_dl, os.X_OK):
				return True
			os.chmod(self.path_youtube_dl, stat.S_IXUSR)
			return True

		
		print(f'Entrando no diretório ... {self.user_prefs.app_local_dirs.dircache()}')
		os.chdir(self.user_prefs.app_local_dirs.dircache())
		print(f'> Conectando ... {self.url_youtube_dl}', end=' ')

		try:
			urllib.request.urlretrieve(self.url_youtube_dl, self.user_prefs.get_youtube_dl_abspath())
		except Exception as err:
			print(__class__.__name__, err)
			return False
		else:
			print('OK')

		if KERNEL_TYPE == 'Linux':
			# Aterar permissão de execução do arquivo se necessário.
			if os.access(self.path_youtube_dl, os.X_OK):
				return True
			os.chmod(self.path_youtube_dl, stat.S_IXUSR)
		elif KERNEL_TYPE == 'Windows':
			print(f'> Baixando visual C x86 ... {self.url_visual_c}')
			try:
				urllib.request.urlretrieve(self.url_visual_c, 'vcredist_x86.exe')
			except Exception as err:
				print(__class__.__name__, err)
				return False
			else:
				print('> Instalando ... vcredist_x86.exe')
				os.system('vcredist_x86.exe')

		return True

	def add_permission(self):
		'''Adicionar permissão de execução no arquivo youtube-dl (chmod +x)'''
		if os.name == 'posix':
			print(f'Alterando permissão do arquivo {self.user_prefs.get_youtube_dl_abspath()}')
			os.system(f'chmod 777 {self.user_prefs.get_youtube_dl_abspath()}')
