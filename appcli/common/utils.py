#!/usr/bin/env python3

import os.path
import json
from pathlib import Path
from shutil import which

from appcli.common.colors import Colors
from appcli.common.apps_conf import (
                                FilePath, 
                                FileReader, 
                                get_term_colums,
                                ExecShellCommand,
                                KERNEL_TYPE,
                                )



def print_line(_char='='):
    print(_char * get_term_colums())

def question(text: str) -> bool:
    """
    :param text: Exibir um texto para o usuário em forma de indagação. 
                 EX: Deseja prosseguir? O ponto de interrogação e colocado 
                 automaticamente por esta função.

    :return: Se o usuário responder s/y com o teclado retorna True, se não retorna False.
    """
    try:
        yesno = str(input(f'{text}? [s/N]: '))
    except(KeyboardInterrupt):
        print('Abortando...')
        return False
    except Exception as e:
        print(e)
        return False
    else:
        if (yesno.lower() == 's') or (yesno.lower() == 'y'):
            return True
        return False


def get_executable_python2() -> str: # -> str ou None
    """
    Retorna o caminho do executável python2 em sistemas posix.
    /usr/bin/python2.7
    /usr/bin/python2
    /usr/bin/python
    """
    executable_python2 = None

    # Detectar python2
    if which('python2.7') is not None:
        executable_python2 = which('python2.7')
    elif which('python2') is not None:
        executable_python2 = which('python2')
    elif which('python') is not None:
        # Verificar versão do executável "python"
        command = ExecShellCommand(['python3', '-V'])
        command.exec_silent()
        if command.text_exit.split()[1][0:3] == '2.7':
            executable_python2 = which('python')

    return executable_python2

class LinuxInfo(object):
    def __init__(self) -> None:
        super().__init__()
        self.file_os_release = FilePath('/etc/os-release')
        
    def __release_lines(self) -> dict:
        """Retorna um dicionário com o conteúdo do arquivo /etc/os-release."""
        info = {}
        file_reader: FileReader = FileReader(self.file_os_release)
        try:
            for line in file_reader.get_lines():
                line: str = line.replace('\n', '').replace('"', '')
                info.update({line.split('=')[0]: line.split('=')[1]})
        except Exception as e:
            print(e)
            return info
        else:
            return info
        finally:
            del file_reader

    def distro_info(self, info: str) -> str:
        """
           Retorna uma informação do sistema com base na chave/info
        os exemplos abaixo são baseados no Debian 11, mas os valore são diferentes
        dependendo da distribuição Linux.


        INFO             - RETORNO
        NAME             - Debian GNU/Linux
        VERSION_ID       - 11
        VERSION 11       - (bullseye)
        VERSION_CODENAME - bullseye
        ID               - debian


        OBSERVAÇÃO:
          Se o parâmetro info não existir o retorno será None, você deve tratar isso.
        """
        os_info = self.__release_lines()
        for k in os_info:
            if k == info:
                return os_info[k]
                break
        return None

    def get_distro_info_json(self):
        """Retorna as informações em formato json"""
        return json.dumps(self.__release_lines(), ensure_ascii=False, indent=4, sort_keys=True)

    def distro_base(self) -> str:
        """
           Retorna o tipo de sistema BASE.
        EX:
        Nos sistemas Debian, Ubuntu, LinuxMint e derivados, o retorno é: debian
        """
        if os.path.isfile('/etc/debian_version'):
            return 'debian'
        else:
            return None
        
    def version_id(self) -> str:
        return self.distro_info('VERSION_ID')

    def version_number(self) -> float:
        """
        SISTEMA    - RETORNO
        Debian 10  - 10
        Debian 11  - 11
        """
        return float(self.distro_info('VERSION_ID'))
        
    def codename(self) -> str:
        if not 'VERSION_CODENAME' in self.__release_lines().keys():
            return None
        return self.distro_info('VERSION_CODENAME')

    def id(self) -> str:
        return self.distro_info('ID')

    def show(self) -> None:
        """Exibe as informações no stdout"""
        print(self.get_distro_info_json())

    
def main():
    pass


    

if __name__ == '__main__':
    main()
