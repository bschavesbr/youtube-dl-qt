#!/usr/bin/env python3

from os import name

if name == 'posix':
	# Default
	RED = '\033[0;31m'
	GREEN = '\033[0;32m'
	YELLOW = '\033[0;33m'
	BLUE = '\033[0;34m'
	WHITE = '\033[0;37m'
	RESET = '\033[m'

	# Strong
	CSRED = '\033[1;31m'
	CSGREEN = '\033[1;32m'
	CSYELLOW = '\033[1;33m'
	CSBLUE = '\033[1;34m'
	CSWHITE = '\033[1;37m'
else:
	# Default
	RED = ''
	GREEN = ''
	YELLOW = ''
	BLUE = ''
	WHITE = ''
	RESET = ''

	# Strong
	CSRED = ''
	CSGREEN = ''
	CSYELLOW = ''
	CSBLUE = ''
	CSWHITE = ''


class Colors(object):
	def __init__(self) -> None:
		self.RED = RED
		self.GREEN = GREEN
		self.YELLOW = YELLOW
		self.BLUE = BLUE
		self.WHITE = WHITE
		self.RESET = RESET


Color = Colors()